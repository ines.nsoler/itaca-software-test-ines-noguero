% Prepare workspace
close all
clc
clear
load ecgConditioningExample.mat

% Parameters
nch = 6;                                % Number of channels

fn = fs/2;                              % Nyquist frequency, used to normalize frequencies. i.e. f_norm= f[Hz]/fn

fc_pl = [49 51];                        % Cutoff frequency [Hz] for Powerline filtering, assuming this frequency is 50Hz

fc_wand = 0.5;                          % HalfPowerFrequency [Hz] for Baseline Wander removal

fc_lp = 70;                             % Cutoff frequency [Hz] for Lowpass filtering

signal_duration=length(ecg(:,1))/fs;    
npeaks = signal_duration*(1/3);         % Estimation of the number of peaks below the channel will be considered as disconnected
                                        % Bradicardic patients = 60 bpm. I estimate the number of peaks with a tolerance. 
t = (0:length(ecg)-1)/fs;               % Temporal array

for i = 1:nch
    % Signal from i channel
    isignal = ecg(:,i);

    %% OFFSET REMOVAL
    isignal = detrend(isignal);

    %% RECORDING SPIKES REMOVAL
    isignal = medfilt1(isignal,4);              % Median filter
    
    %% BASELINE WANDER 
    [z,p] = butter(4,fc_wand/fn,'high');        % Butterworth high-pass filter
    %fvtool(z,p,'Fs',fs);               % In case I want to see the filter response
    isignal = filtfilt(z,p,isignal);            % filtfilt tool performs zero-phase digital filtering 
            
    %% LOW-PASS FILTERING 
    [bn,an] = butter(4,fc_lp/fn,'low');         % Butterworth low-pass filter
    %fvtool(bn,an,'Fs',fs);                     
    isignal=filtfilt(bn,an,isignal);  
    
    %% POWERLINE INTERFERENCE FILTERING
    [b,a] = butter(2,fc_pl/fn,'stop');          % Butterworth stop-band filter
    isignal=filtfilt(b,a,isignal);
            
    %% PLOTTING    
    subplot(nch,1,i)                            % Preprocessed signal is displayed
    plot(t,isignal);
    ylabel('Amplitude')
    xlabel('Time')

    %% DETECTION OF NON CONNECTED CHANNELS
    peaks = length(findpeaks(isignal,'MinPeakHeight',(2*max(isignal)/3)));       % Get the peaks in the signal that are higher from a certain threshold
                                                                                 % This threshold is estimated as 2/3 of the signal max amplitude
    if peaks<npeaks                                                              % I use the parameter npeaks, calculated at the beggining of the code 
        warning("Channel "+ i + " is not connected")
    end
end